// caller contract

#include <eosiolib/eosio.hpp>
#include <string>

using namespace eosio;
using namespace std;

// @abi table
struct data
{
   uint64_t id;
   name  consumer;
   string number;
   string serialno;

   uint64_t primary_key() const { return id; }

   EOSLIB_SERIALIZE( data, (id) (consumer) (number) (serialno) )
};

typedef eosio::multi_index<N(data), data> tb_data;

class caller : public eosio::contract {
  public:
      using contract::contract;

      /// @abi action 
      void deliver(account_name consumer, string serialno, account_name caller, string random){
         if (caller != _self || consumer != _self) {
            return;
         }

         require_auth(N(randomoracle));   // random contract account
         //print( "Number: ", random );

         tb_data d(_self, _self);
         uint64_t i = 0;
         if (d.rbegin() != d.rend()) {
            i = d.rbegin()->id + 1;
         }

         d.emplace(_self, [&](auto& a){
            a.id = i;
            a.consumer.value = consumer;
            a.number = random;
            a.serialno = serialno;
         });
      }

      // @abi action
      void foobar()
      {
         require_auth(_self);

         tb_data d(_self, _self);
         for (auto i = d.begin(); i != d.end(); ) {
            i = d.erase(i);
         }
      }
};

#define EOSIO_ABI_EX( TYPE, MEMBERS ) \
extern "C" { \
    void apply( uint64_t receiver, uint64_t code, uint64_t action ) { \
        auto self = receiver; \
        if( action == N(onerror)) { \
            /* onerror is only valid if it is for the "eosio" code account and authorized by "eosio"'s "active permission */ \
            eosio_assert(code == N(eosio), "onerror action's are only valid from the \"eosio\" system account"); \
        } \
        if((code == receiver && action != N(deliver)) \
            || (code == N(randomoracle) && action == N(deliver))) { \
            TYPE thiscontract( self ); \
            switch( action ) { \
                EOSIO_API( TYPE, MEMBERS ) \
            } \
         /* does not allow destructor of thiscontract to run: eosio_exit(0); */ \
        } \
    } \
} \

EOSIO_ABI_EX( caller, (deliver) (foobar) )
