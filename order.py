# -*- coding: UTF-8 -*-
# This script should run by Python3

import random
import sys
import hashlib
import os
import time

# You should change this consumername to your own eos account
user_name = "consumername"

# node url, change it if you find the network is down or too slow
node_url = "https://api-kylin.eoslaomao.com"

contract_name = 'numberrandom'

cmd1 = "cleos -u "
cmd2 = " push action "
cmd3 = "'["
cmd4 = "]' -p "

# wait second to make order again
wait_seconds = 3600

def make_order(serialno, caller):
    memo = "ORDER:" + serialno + "|" + caller
    cmd = cmd1 + node_url + "  push action octtothemoon transfer " + cmd3 + '"' + user_name + '","' + contract_name + '","1.0000 OCT","' + memo + '"' + cmd4 + user_name
    print(cmd)
    os.system(cmd)

if __name__ == '__main__':
    while True:
        print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
        temp = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8')).hexdigest()
        make_order(temp[0:32], user_name)

        time.sleep(wait_seconds)