# -*- coding: UTF-8 -*-
# This script should run by Python3
# And it's built for oraclechain's random datafeeder
# Datafeeder should change *user_name* as his/her eos account
# And delegate more CPU to account
# Datafeeder should stop this script by CTRL+C
# And it's not recommand to do so when there are orders valid
# Datafeeder should stop script when there is no order

import urllib.request
from urllib.error import HTTPError
from urllib.error import URLError
import json
import random
import sys
import hashlib
import time
import copy
import subprocess

# You should change this datafeeder11 to your own eos account
user_name = "oraclefeeder"

# node url, change it if you find the network is down or too slow
nodes = [
    "https://api.eoslaomao.com",
    "https://eos.newdex.one",
    "http://peer1.eoshuobipool.com:8181",
    "https://node1.zbeos.com",
    "https://api.bitmars.one",
    "https://mainnet.meet.one",
    "https://mainnet.eoscannon.io",
    "https://api.helloeos.com.cn"
]

get_table_rows_url = "/v1/chain/get_table_rows"

contract_name = 'randomoracle'

# wait second to query order
wait_seconds = 5

datafeeder_limit = 6

table_param = {
    "code": "",
    "json": 1,
    "limit": -1,
    "lower_bound": "",
    "scope": "",
    "table": "",
    "table_key": "",
    "upper_bound": ""
}

headers = {
    'Content-Type': 'application/json',
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'
}

class record:
    def __init__(self):
        self.round = 0
        self.hm = ""
        self.msg = ""

records = []

def make_request(url, param):
    request = urllib.request.Request(url=url, headers=headers, data=json.dumps(param).encode(encoding='UTF8'))
    try:
        response = urllib.request.urlopen(request, timeout=10)
        info = response.read().decode('utf8', "ignore")
        return info
    except HTTPError as e:
        print(e.code, e.msg)
    except URLError as e:
        print(e.reason)
    return {}

def query_table(param):
    for url in nodes:
        info = make_request(url + get_table_rows_url, param)
        if len(info):
            resp = json.loads(info)
            if resp["rows"]:
                return resp["rows"]
    return []

def query_window(round, limit):
    window_param = copy.deepcopy(table_param)
    window_param["table"] = "window"
    window_param["lower_bound"] = round
    window_param["limit"] = limit
    return query_table(window_param)

def make_push(action, round, value):
    arg = []
    arg.append("cleos")
    arg.append("-u")
    arg.append("node_url")
    arg.append("push")
    arg.append("action")
    arg.append(contract_name)
    arg.append(action)
    arg.append('["' + user_name + '",' + str(round) + ',"' + value + '"]')
    arg.append("-p")
    arg.append(user_name)

    for url in nodes:
        arg[2] = url
        print(arg)
        process = subprocess.Popen(arg)
        rc = 1
        try:
            rc = process.wait(timeout=10)
        except subprocess.TimeoutExpired:
            process.kill()
        print("return code: ", rc)
        if rc == 0:
            break


def make_commit(round, hm):
    print("Making commit to window", round, "...")
    make_push("makecommit", round, hm)

def make_reveal(round, msg):
    print("Making reveal to window", round, "...")
    make_push("makereveal", round, msg)

def is_df_commited(commits):
    if len(commits) != 0:
        for c in commits:
            if c["datafeeder"] == user_name:
                return True
    return False

def is_df_revealed(commits):
    if len(commits) != 0:
        for c in commits:
            if ( c["datafeeder"] == user_name and c["msg"] != "" ):
                return True
    return False

def is_commit_valid(round):
    window = query_window(round, 1)
    if len(window) == 0:
        return False
    else:
        commits = window[0]["commits"]
        if len(commits) != 0:
            if is_df_commited(commits):
                return False
            if len(commits) == datafeeder_limit:
                return False
    return True

def is_reveal_valid(round):
    window = query_window(round, 1)
    if len(window) == 0:
        return False
    else:
        commits = window[0]["commits"]
        if (len(commits) == 0 or len(commits) != datafeeder_limit):
            return False
        if is_df_commited(commits) == False:
            return False
        if is_df_revealed(commits):
            return False
    return True

def is_round_record(round):
    for r in records:
        if r.round == round:
            return True
    return False

def gene_rand(prerandom, round):
    r = record()
    r.round = round
    while True:
        msg = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8')).hexdigest()
        hm = hashlib.sha256((str(round) + prerandom + msg ).encode('utf-8')).hexdigest()
        if int(hm[0:4], 16) == round % 0xffff:
            r.hm = hm
            r.msg = msg
            break
    return r

def keep_wallet_unlocked():
    print("keep wallet unlocked...")
    process = subprocess.Popen(['cleos', 'wallet', 'list'])
    process.wait(timeout=10)

if __name__ == '__main__':
    table_param["code"] = contract_name
    table_param["scope"] = contract_name

    order_param = copy.deepcopy(table_param)
    order_param["table"] = "ordering"

    while True:
        print("---------------------------------------------------------------------")
        print("Querying orders...")
        orderings = query_table(order_param)
        if len(orderings):
            # order exists, we should process
            print("Found orders, now start processing...")
            windows = query_window("", -1)
            if len(windows):
                #print(json.dumps(windows, indent=4, sort_keys=True))
                rounds = []
                for w in windows:
                    round = w["round"]
                    rounds.append(round)
                    if is_round_record(round) == False:
                        r = gene_rand(w["prerandom"], round)
                        records.append(r)
                # some local round already invalid on line, so remove them
                for r in records:
                    if r.round not in rounds:
                        records.remove(r)

            print("\n*****Local data*****")
            for r in records:
                print("round:", r.round)
                print("hm:", r.hm)
                print("msg:", r.msg)
                print("--------------------------------")
            
            # first send all commits
            commited = False
            for r in records:
                if is_commit_valid(r.round):
                    make_commit(r.round, r.hm)
                    commited = True
            if commited == False:
                print("No commit made, cause you already made or window is full")

            # then send all reveals
            revealed = False
            for r in records:
                if is_reveal_valid(r.round):
                    make_reveal(r.round, r.msg)
                    revealed = True
            if revealed == False:
                print("No reveal made, cause you already made or commit is not full or you not commit before")
        else:
            print("No orders, wait", wait_seconds, "seconds and try agian.")
            time.sleep(wait_seconds)

        # keep wallet unlocked
        keep_wallet_unlocked()
