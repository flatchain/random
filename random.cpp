#include "random.hpp"
#include <eosiolib/crypto.h>
#include <eosiolib/types.h>
#include <eosiolib/transaction.hpp>
#include <boost/algorithm/string.hpp>

#define DF_REGISTER "REGISTER:"
#define MAKE_ORDER "ORDER:"
#define MEMO_SPLITTER "|"

const static int64_t datafeeder_registery_fee = 1;
const static int64_t consumer_order_fee = 10000;
const static int32_t window_limit = 5;
const static int32_t datafeeder_bench = 6;
const static int32_t order_process_limit = 3;
const static uint32_t process_order_delay_seconds = 1;
const static uint32_t ordered_limit = 20;
const static uint32_t patrol_delay_seconds = 600;
const static uint32_t order_active_check_seconds = 120;

random::random(account_name name) 
    : contract(name),
    windows(_self, _self),
    orderings(_self, _self),
    ordereds(_self, _self),
    datafeeders(_self, _self),
    configs(_self, _self)
{}

bool random::order_equal(const order& o1, const order& o2)
{
    return ( o1.consumer == o2.consumer )
        && ( o1.serialno == o2.serialno );
}

bool random::order_exist(const order& o)
{
    for (auto od = orderings.begin(); od != orderings.end(); ++od) {
        if (order_equal(*od, o)) {
            return true;
        }
    }
    return false;
}

void random::gen_one_window(uint64_t round, const string& prerandom)
{
    auto w = windows.find(round);
    eosio_assert(w == windows.end(), "window already exists.");

    windows.emplace(_self, [&](auto& a){
        a.round = round;
        a.prerandom = prerandom;
    });
}

void random::erase_lower_window(uint64_t round5, uint64_t last_round, const string& seed)
{
    // we erase all round5 - 5 window
    if (round5 >= window_limit + 1) {
        for (auto w = windows.begin(); w != windows.end(); ) {
            if (w->round < round5 - window_limit) {
                w = windows.erase(w);
            } else {
                ++w;
            }
        }
    }

    //  make up to 5 window
    int need_supply = window_limit - window_count();
    for (auto i = 0; i < need_supply; ++i) {
        gen_one_window(last_round + 1 + i, new_prerandom(seed, i + 1));
    }
}

string random::cal_hm(uint64_t round, const string& prerandom, const string& msg)
{
    string h;
    char str[255] = { 0 };
    snprintf(str, sizeof(str), "%lld%s%s", round, prerandom.c_str(), msg.c_str());
    checksum256 cs = { 0 };
    sha256(str, strlen(str), &cs);

    for (int i = 0; i < sizeof(cs.hash); ++i) {
        char hex[3] = { 0 };
        snprintf(hex, sizeof(hex), "%02x", static_cast<unsigned char>(cs.hash[i]));
        h += hex;
    }

    return h;
}

string random::hexDec2String(int hexIn)
{
	char hexString[4 * sizeof(int) + 1] = { 0 };
	snprintf(hexString, sizeof(hexString), "%x", hexIn);
	return string(hexString);
}

string random::xor_code(const string& str1, const string& str2)
{
    string res;
    if (str1.size() != str2.size()) {
        return res;
    }

    for (auto i = 0; i < str1.size(); ++i) {
        // plan A
        char c = str1.at(i) ^ str2.at(i);
        res.push_back(c);

        // plan B
        //int c = stoi(str1.substr(i, 1), nullptr, 16) ^ stoi(str2.substr(i, 1), nullptr, 16);
		//res.append(hexDec2String(c));
    }

    return res;
}

string binary2string(const string& src)
{
    string res;
    for (auto i = 0; i < src.size(); ++i) {
        //int c = stoi(src.substr(i, 1), nullptr, 16);
        char hexString[4 * sizeof(int) + 1] = { 0 };
	    snprintf(hexString, sizeof(hexString), "%x", src.at(i) % 16);
        res.push_back(hexString[0]);
    }

    return res;
}

string random::cal_random(const string& seed, account_name consumer, const string& serialno, account_name caller, uint64_t ordtime)
{
    string h;
    char str[255] = { 0 };
    snprintf(str, sizeof(str), "%s%lld%s%lld%lld", seed.c_str(),
            consumer, serialno.c_str(), caller, ordtime);
    checksum256 cs = { 0 };
    sha256(str, strlen(str), &cs);

    for (int i = 0; i < sizeof(cs.hash); ++i) {
        char hex[3] = { 0 };
        snprintf(hex, sizeof(hex), "%02x", static_cast<unsigned char>(cs.hash[i]));
        h += hex;
    }

    return h;
}

int random::window_count()
{
    int cnt = 0;
    for (auto w = windows.begin(); w != windows.end(); ++w) {
        cnt++;
    }

    return cnt;
}

string random::new_prerandom(const string& seed, int i)
{
    //  seed + 1
    string h = seed;
	int last_word = (stoi(seed.substr(seed.length() - 1, 1), nullptr, 16) + i) % 0xf;
	string last_char = hexDec2String(last_word);

	return h.replace(h.end() - 1, h.end(), last_char);
}

bool random::parse_memo_order(const string& memo, string& serialno, account_name& caller)
{
    if (memo.empty()) {
        return false;
    }

    vector<string> strs;
    boost::split(strs, memo, boost::is_any_of(MEMO_SPLITTER));
    if (strs.size() < 2) {
        return false;
    }

    serialno = strs.at(0);
    if (serialno.empty()) {
        return false;
    }

    string cs = strs.at(1);
    if (cs.empty()) {
        return false;
    }

    caller = string_to_name(cs.c_str());

    return true;
}

void random::parse_memo_register(const string& memo, string& image, string& site)
{
    if (memo.empty()) {
        return;
    }

    vector<string> strs;
    boost::split(strs, memo, boost::is_any_of(MEMO_SPLITTER));
    if (strs.size() < 2) {
        return;
    }

    image = strs.at(0);
    site = strs.at(1);
}

void random::check_window_active(uint32_t time)
{
    if (configs.begin() == configs.end()) {
        return;
    }

    auto first_order = orderings.begin();
    if (first_order == orderings.end()) {
        return;
    }

    if (time - (uint32_t)(first_order->ordtime / 1000) > order_active_check_seconds) {
        auto w = windows.begin();
        if (w == windows.end()) {
            return;
        }

        if (w->commits.size() < datafeeder_bench) {
            return;
        }

        uint64_t max_round = windows.rbegin()->round;
        gen_one_window(max_round + 1, w->prerandom);
        windows.erase(w);
    }
}

void random::init(string initseed)
{
    require_auth(_self);

    auto c = configs.begin();
    if (c == configs.end()) {
        configs.emplace(_self, [&](auto& a){
            a.owner.value = _self;
            
            asset zero_oct{0, S(4,OCT)};
            a.staked = zero_oct;
            a.allrwds = zero_oct;
            a.reward = zero_oct;

            a.on = true;
            a.reg = true;
            a.ord = true;

            a.orders = 0;
            a.ordered = 0;
        });
    }

    // initialize 5 windows
    auto w = windows.begin();
    eosio_assert(w == windows.end(), "contract already initialized.");
    eosio_assert(initseed.size() == 64, "initseed must be 64 size string.");

    // for convenience, we start from 1.
    for (auto i = 1; i <= 5; ++i) {
        gen_one_window(i, initseed);
    }
}

void random::reviewdf(account_name df, int8_t state)
{
    require_auth(_self);

    auto c = configs.begin();
    eosio_assert(c != configs.end(), "contract has not been initialized yet.");

    auto d = datafeeders.find(df);
    eosio_assert(d != datafeeders.end(), "datafeeder not exists.");

    // not passed, we send back df's staked OCT, and erase this data.
    if (state == registering) {
        action(
            permission_level{ _self, N(active) },
                N(octtothemoon), N(transfer),
                std::make_tuple(_self, df, d->staked, std::string("Sorry that you have not passed our review."))
        ).send();

        configs.modify(*c, _self, [&](auto& a){
            a.staked -= d->staked;
        });

        datafeeders.erase(d);

        return;
    }

    //  either registered or forzen
    datafeeders.modify(*d, _self, [&](auto& a){
        a.state = state;
    });
}

void random::closewnd(uint64_t round, string initseed)
{
    require_auth(_self);

    auto w = windows.find(round);
    eosio_assert(w != windows.end(), "window not exists.");
    eosio_assert(initseed.size() == 64, "initseed must be 64 size string.");

    uint64_t max_round = windows.rbegin()->round;
    windows.erase(w);
    gen_one_window(max_round + 1, initseed);
}

void random::closeord(uint64_t index, bool recede)
{
    require_auth(_self);

    auto o = orderings.find(index);
    eosio_assert(o != orderings.end(), "order not exists.");

    if (recede) {
        action(
            permission_level{ _self, N(active) },
                N(octtothemoon), N(transfer),
                std::make_tuple(_self, o->consumer, o->fee, std::string("Recede your cost OCT, you must implement the deliver action properly."))
        ).send();
    }
    orderings.erase(o);
}

void random::make_order(account_name consumer, string serialno, account_name caller, asset quantity)
{
    require_auth(consumer);

    auto c = configs.begin();
    eosio_assert(c != configs.end(), "contract has not been initialized yet.");
    eosio_assert(c->on && c->ord, "you can't make order for now.");

    //  save order, and consumer pay ram.
    eosio_assert(serialno.size() == 32, "serial number must be a 32 size string.");
    eosio_assert(is_account(caller), "caller must be account.");

    order o;
    o.consumer.value = consumer;
    o.serialno = serialno;
    o.caller.value = caller;

    eosio_assert(!order_exist(o), "same order already exists.");
    uint64_t new_index = 0;
    auto last_order = orderings.rbegin();
    if (last_order != orderings.rend()) {
        new_index = last_order->index + 1;
    }

    orderings.emplace(_self, [&](auto& a){
        a.index = new_index;
        a.consumer.value = consumer;
        a.serialno = serialno;
        a.caller.value = caller;
        a.fee = quantity;
        a.ordtime = current_time() / 1000;
        a.protime = 0;
    });
}

void random::makecommit(account_name df, uint64_t round, string hm)
{
    require_auth(df);

    auto c = configs.begin();
    eosio_assert(c != configs.end(), "contract has not been initialized yet.");
    eosio_assert(c->on, "you can't make commit for now.");

    auto d = datafeeders.find(df);
    eosio_assert(d != datafeeders.end(), "you must register datafeeder first.");
    eosio_assert(d->state == registered, "you can't make commit cause you are not a datafeeder yet.");

    //  save commit
    auto w = windows.find(round);
    eosio_assert(w != windows.end(), "window not exists.");
    eosio_assert(hm.size() == 64, "hm must be 64 size string.");

    auto commits = w->commits;
    eosio_assert(commits.size() < datafeeder_bench, "commits is full, try other window.");

    name feeder;
    feeder.value = df;
    for (const auto& c : commits) {
        if (c.datafeeder == feeder) {
            eosio_assert(false, "you already commit to this window.");
        }
    }

    int two_b = strtoll(hm.substr(0, 4).c_str(), nullptr, 16);
    eosio_assert(two_b == round % 0xffff, "your hm deos not match round % 0xffff.");

    commit cmt;
    cmt.datafeeder = feeder;
    cmt.hm = hm;

    commits.push_back(cmt);

    windows.modify(*w, _self, [&](auto& a){
        a.commits = commits;
    });
}

void random::makereveal(account_name df, uint64_t round, string msg)
{
    require_auth(df);

    auto c = configs.begin();
    eosio_assert(c != configs.end(), "contract has not been initialized yet.");
    eosio_assert(c->on, "you can't make reveal for now.");

    auto d = datafeeders.find(df);
    eosio_assert(d != datafeeders.end(), "you must register datafeeder first.");
    eosio_assert(d->state == registered, "you can't make reveal cause you are not a datafeeder yet.");

    // save reveal, and generate seed, and generate random number, and send number to caller.

    auto w = windows.find(round);
    eosio_assert(w != windows.end(), "window not exists.");
    eosio_assert(msg.size() == 64, "msg must be 64 size string.");

    auto commits = w->commits;
    eosio_assert(commits.size() >= datafeeder_bench, "commits is not full yet.");

    bool found = false;
    int32_t reveal_count = 0;
    name feeder;
    feeder.value = df;
    string hm = cal_hm(round, w->prerandom, msg);
    for (auto& c : commits) {
        if (c.datafeeder == feeder && hm == c.hm) {
            c.msg = msg;
            found = true;
        }

        if (!c.msg.empty()) {
            reveal_count++;
        }
    }

    eosio_assert(found, "reveal doesn't match commit.");

    uint64_t max_round = windows.rbegin()->round;

    if (reveal_count != datafeeder_bench) {
        windows.modify(*w, _self, [&](auto& a){
            a.commits = commits;
        });
    } else {
        //  generate seed.
        string seed, msg;
        for (const auto& c : commits) {
            if (seed.empty()) {
                if (!msg.empty()) {
                    seed = msg;
                }
            } else {
                seed = xor_code(seed, msg);
            }

            msg = c.msg;
        }

        //  use seed to generate random number, and send it to code.
        seed = binary2string(seed);
        int32_t count = 0;
        uint64_t now_msec = current_time() / 1000;
        for (auto od = orderings.begin(); od != orderings.end() && count < order_process_limit; ++od) {
            if ((od->ordtime + 500 < now_msec ) && (od->protime + process_order_delay_seconds * 1000 < now_msec)) {
                uint128_t sender_id = uint128_t(_self + od->index);
                transaction out;
                out.actions.emplace_back( permission_level{ _self, N(active) },
                                            _self, N(processorder),
                                            std::make_tuple(od->consumer, od->serialno, od->caller, od->ordtime, seed)
                );
                out.delay_sec = process_order_delay_seconds;
                cancel_deferred(sender_id);
                out.send(sender_id, df, true);

                orderings.modify(*od, _self, [&](auto& a){
                    a.protime = now_msec;
                });

                count++;
            }
        }

        //  erase current window
        windows.erase(w);

        //  erase lower window < round - 5, and make up to 5 window.
        erase_lower_window(round, max_round, seed);
    }
}

void random::processorder(account_name consumer, const string& serialno, account_name caller, uint64_t ordtime, const string& seed)
{
    require_auth(_self);

    auto c = configs.begin();
    eosio_assert(c != configs.end(), "contract has not been initialized yet.");
    eosio_assert(c->on, "you can't process order for now.");

    order od;
    od.consumer.value = consumer;
    od.serialno = serialno;
    od.caller.value = caller;

    auto o = orderings.end();
    for (o = orderings.begin(); o!= orderings.end(); ++o) {
        if (order_equal(od, *o)) {
            od.ordtime = o->ordtime;
            break;
        }
    }

    eosio_assert(o != orderings.end(), "order not exists.");
    eosio_assert(o->protime + process_order_delay_seconds * 1000 <= current_time() / 1000, "process is not available yet.");

    string number = cal_random(seed, consumer, serialno, caller, ordtime);
    action(
        permission_level{ _self, N(active) },
        _self, N(deliver),
        std::make_tuple(consumer, serialno, caller, number)
    ).send();

    orderings.erase(o);

    int32_t cnt = c->ordered;
    for (auto o = ordereds.begin(); o != ordereds.end() && cnt >= ordered_limit; ) {
        o = ordereds.erase(o);
        cnt--;
    }

    int64_t index = 1;
    if (ordereds.rbegin() != ordereds.rend()) {
        index = ordereds.rbegin()->index + 1;
    }

    ordereds.emplace(_self, [&](auto& a){
        a.index = index;
        a.consumer = od.consumer;
        a.serialno = od.serialno;
        a.caller = od.caller;
        a.fee = o->fee;
        a.ordtime = od.ordtime;
        a.protime = current_time() / 1000;
        a.random = number;
    });

    cnt += 1;

    configs.modify(*c, _self, [&](auto& a){
        a.orders += 1;
        a.ordered = cnt;
    });
}

void random::transfer(account_name from, account_name to, asset quantity, string memo)
{
    if (from == _self || to != _self) {
        return;
    }

    require_auth(from);

    eosio_assert(quantity.symbol == S(4,OCT), "only OCT support.");
    eosio_assert(quantity.is_valid(), "invalid token transfer.");
    eosio_assert(quantity.amount > 0, "quantity must be positive.");

    auto c = configs.begin();
    eosio_assert(c != configs.end(), "contract has not been initialized yet.");

    if (memo.find(DF_REGISTER) == 0) {
        //  memo should be:REGISTER:image|url
        eosio_assert(c->on && c->reg, "you can't register datefeeder for now.");
        
        eosio_assert(quantity.amount >= datafeeder_registery_fee, "valid quantity failed.");
        auto d = datafeeders.find(from);
        eosio_assert(d == datafeeders.end(), "you have already submitted your registery application, just wait for a while.");
        string img, site, rg = memo.substr(string(DF_REGISTER).size());
        parse_memo_register(rg, img, site);
        datafeeders.emplace(_self, [&](auto& a){
            a.id.value = from;
            a.staked = quantity;
            
            asset zero_oct{0, S(4,OCT)};
            a.allrwds = zero_oct;
            a.reward = zero_oct;

            a.state = 0;
            a.order = 0;
            a.time = 0;
            a.image = img;
            a.site = site;
        });

        configs.modify(*c, _self, [&](auto& a){
            a.staked += quantity;
        });
    } else if (memo.find(MAKE_ORDER) == 0) {
        //  consumer make order for random number
        //  memo should be:ORDER:serialno|caller
        eosio_assert(c->on && c->ord, "you can't make order for now.");

        eosio_assert(quantity.amount >= consumer_order_fee, "not enough OCT to make order for random.");
        string serialno, ss = memo.substr(string(MAKE_ORDER).size());
        account_name caller;
        eosio_assert(parse_memo_order(ss, serialno, caller), "wrong format memo of make order of random.");
        make_order(from, serialno, caller, quantity);

        configs.modify(*c, _self, [&](auto& a){
            a.allrwds += quantity;
            a.reward += quantity;
        });
    }
}

void random::logout(account_name df)
{
    require_auth(df);

    auto c = configs.begin();
    eosio_assert(c != configs.end(), "contract has not been initialized yet.");
    eosio_assert(c->on, "you can't logout for now.");

    auto d = datafeeders.find(df);
    eosio_assert(d != datafeeders.end(), "you have not registered for datafeeder yet.");
    eosio_assert(d->reward.amount == 0, "you must claim your rewards first before logout.");

    action(
        permission_level{ _self, N(active) },
            N(octtothemoon), N(transfer),
            std::make_tuple(_self, df, d->staked, std::string("Thank you for been a datafeeder, hope to see you again."))
    ).send();

    configs.modify(*c, _self, [&](auto& a){
        a.staked -= d->staked;
    });

    datafeeders.erase(d);
}

void random::deliver(account_name consumer, string serialno, account_name caller, string random)
{
    require_auth(_self);
    require_recipient(caller);
}

void random::patrol(uint32_t time)
{
    require_auth(_self);

    check_window_active(time);

    uint128_t sender_id = uint128_t(_self - 1);
    transaction out;
    out.actions.emplace_back( permission_level{ _self, N(active) },
                                _self, N(patrol),
                                std::make_tuple(now())
    );
    out.delay_sec = patrol_delay_seconds;
    cancel_deferred(sender_id);
    out.send(sender_id, _self, true);
}

void random::updatedf(account_name df, const string& image, const string& site)
{
    require_auth(df);

    auto c = configs.begin();
    eosio_assert(c != configs.end(), "contract has not been initialized yet.");

    auto d = datafeeders.find(df);
    eosio_assert(d != datafeeders.end(), "you are not registered yet.");
    datafeeders.modify(*d, _self, [&](auto& a) {
        a.image = image;
        a.site = site;
    });
}

void random::foobar()
{
    require_auth(_self);

    /*for (auto w = windows.begin(); w != windows.end(); ) {
        w = windows.erase(w);
    }

    for (auto oi = orderings.begin(); oi != orderings.end(); ) {
        oi = orderings.erase(oi);
    }

    for (auto od = ordereds.begin(); od != ordereds.end(); ) {
        od = ordereds.erase(od);
    }

    for (auto d = datafeeders.begin(); d != datafeeders.end(); ) {
        d = datafeeders.erase(d);
    }

    for (auto c = configs.begin(); c != configs.end(); ) {
        c = configs.erase(c);
    }*/

    auto c = configs.begin();
    if (c != configs.end()) {
        configs.modify(*c, _self, [&](auto& a){
            a.ordered = 1;
        });
    }
}

#define EOSIO_ABI_EX( TYPE, MEMBERS ) \
extern "C" { \
    void apply( uint64_t receiver, uint64_t code, uint64_t action ) { \
        auto self = receiver; \
        if( action == N(onerror)) { \
            /* onerror is only valid if it is for the "eosio" code account and authorized by "eosio"'s "active permission */ \
            eosio_assert(code == N(eosio), "onerror action's are only valid from the \"eosio\" system account"); \
        } \
        if((code == receiver && action != N(transfer)) \
            || (code == N(octtothemoon) && action == N(transfer))) { \
            TYPE thiscontract( self ); \
            switch( action ) { \
                EOSIO_API( TYPE, MEMBERS ) \
            } \
         /* does not allow destructor of thiscontract to run: eosio_exit(0); */ \
        } \
    } \
} \

EOSIO_ABI_EX( random, (init) (reviewdf) (closewnd) (closeord) (makecommit) (makereveal) (processorder) (transfer) (logout) (foobar) (deliver) (patrol) (updatedf) )