// This is random generator contract.
#include <eosiolib/eosio.hpp>
#include <eosiolib/asset.hpp>
#include <string>
#include <vector>

using namespace eosio;
using namespace std;

struct commit
{
    name    datafeeder;
    string  hm;
    string  msg;
};

//  @abi table
struct window
{
    uint64_t        round;
    string          prerandom;
    vector<commit>  commits;

    uint64_t primary_key() const { return round; }

    EOSLIB_SERIALIZE( window, (round) (prerandom) (commits) )
};

typedef eosio::multi_index<N(window), window> tb_window;

//  @abi table
struct order
{
    uint64_t    index;     //   auto-increase
    name        consumer;
    string      serialno;
    name        caller;
    asset       fee;       //   cost
    uint64_t    ordtime;   //   msec, make order time
    int64_t     protime;   //   msec, start process derfer send time.
    string      random;    //   random number that generated.

    uint64_t primary_key() const { return index; }

    EOSLIB_SERIALIZE( order, (index) (consumer) (serialno) (caller) (fee) (ordtime) (protime) (random) )
};

typedef eosio::multi_index<N(ordering), order> tb_ordering;
typedef eosio::multi_index<N(ordered), order> tb_ordered;

enum {
    registering = 0,
    registered = 1,
    frozen = 2
};

//  @abi table
struct datafeeder
{
    name        id;
    asset       staked;     //  staked OCT
    asset       allrwds;    //  whole rewards
    asset       reward;     //  valid reward
    int8_t      state;      //  0 for registering, 1 for registered, 2 for frozen
    int32_t     order;      //  how many orders does datafeeder provide
    int64_t     time;       //  last time to submit data
    string      image;      //  image url
    string      site;       //  site url

    uint64_t    primary_key() const { return id.value; }

    EOSLIB_SERIALIZE( datafeeder, (id) (staked) (allrwds) (reward) (state) (order) (time) (image) (site) )
};

typedef eosio::multi_index<N(datafeeder), datafeeder> tb_datafeeder;

//  @abi table
struct config
{
    name        owner;      //  _self
    asset       staked;     //  whole staked OCT
    asset       allrwds;    //  whole rewards
    asset       reward;     //  valid rewards

    bool        on;         //  on/off
    bool        reg;        //  0 for not allow datafeeder to register, 1 for yes
    bool        ord;        //  0 for not allow consumer to make order, 1 for yes

    int32_t     orders;     //  total finished orders
    int32_t     ordered;    //  number of order in ordered table

    uint64_t primary_key() const { return owner.value; }

    EOSLIB_SERIALIZE( config, (owner) (staked) (allrwds) (reward) (on) (reg) (ord) (orders) (ordered) )
};

typedef eosio::multi_index<N(config), config> tb_config;