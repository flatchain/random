# random2 contract
A random contract of EOS implemented by [oraclechain](https://github.com/oraclechain).

## usage
1. for administrator

initialize contract:

```
EOSTEST push action numberrandom init '["c08c69e9dd9259c231877497b1264cc484bb3e5c71e039382e07a4ee8c896ec6"]' -p numberrandom
```

review datafeeder: (0 for remove, 1 for approve, 2 for frozen)

```
EOSTEST push action numberrandom reviewdf '["datafeeder55", 1]' -p numberrandom
```


2. for consumer (make order)

```
EOSTEST push action octtothemoon transfer '["consumername", "numberrandom", "10.0000 OCT", "ORDER:73e0a177872834041380d7f19205e774026111d9d274375664afa54e4fac351d|consumername"]' -p consumername
```

caller must implement `deliver` action, and give `numberrandom` auth, and himself must has eosio.code permission.

3. for datafeeder

register:

```
EOSTEST push action octtothemoon transfer '["datafeeder55", "numberrandom", "0.0001 OCT", "REGISTER:https://oraclechain.io/images/logo.png|https://oraclechain.io"]' -p datafeeder55
```

```
EOSTEST push action numberrandom makecommit '["datafeeder44", 1, "0100a622b773680fc7a71297cd2c90b24e86d7d1a79e243d8bf23e6578b88a77"]' -p datafeeder44
```

```
EOSTEST push action numberrandom makereveal '["datafeeder44", 1, "20b9dc22d0974f01bac5f5378a7332bf13c7aeb1eaba7d65ef2fd2d678f8e724"]' -p datafeeder44
```
