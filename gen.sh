rm -rf *.was* *.abi

eosiocpp -o random.wast random.cpp
eosiocpp -g random.abi random.cpp

eosiocpp -o caller.wast caller.cpp
eosiocpp -g caller.abi caller.cpp
