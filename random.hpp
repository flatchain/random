#include "table.hpp"

class random : public contract
{
public:
    random(account_name name);

    //  for administrator
    //  @abi action
    void init(string initseed);

    //  @abi action
    void reviewdf(account_name df, int8_t state);

    //  @abi action
    void closewnd(uint64_t round, string initseed);

    //  @abi action
    void closeord(uint64_t index, bool recede);

    // for datafeeder
    //  @abi action
    void makecommit(account_name df, uint64_t round, string hm);

    //  @abi action
    void makereveal(account_name df, uint64_t round, string msg);

    //  @abi action
    void processorder(account_name consumer, const string& serialno, account_name caller, uint64_t ordtime, const string& seed);

    //  for datafeeder and consumer
    //  @abi action
    void transfer(account_name from, account_name to, asset quantity, string memo);

    //  @abi action
    void logout(account_name df);

    //  @abi action
    void deliver(account_name consumer, string serialno, account_name caller, string random);

    //  @abi action
    void patrol(uint32_t time);

    // @abi action
    void updatedf(account_name df, const string& image, const string& site);

    //  for Debug
    //  @abi action
    void foobar();

private:
    bool order_equal(const order& o1, const order& o2);
    bool order_exist(const order& o);

    void gen_one_window(uint64_t round, const string& prerandom);
    void erase_lower_window(uint64_t round5, uint64_t last_round, const string& seed);

    string cal_hm(uint64_t round, const string& prerandom, const string& msg);
    string cal_random(const string& seed, account_name consumer, const string& serialno, account_name caller, uint64_t ordtime);
    string xor_code(const string& str1, const string& str2);
    string new_prerandom(const string& seed, int i);
    string hexDec2String(int hexIn);

    int window_count();

    bool parse_memo_order(const string& memo, string& serialno, account_name& caller);
    void parse_memo_register(const string& memo, string& image, string& site);
    void make_order(account_name consumer, string serialno, account_name caller, asset quantity);

    void check_window_active(uint32_t time);

private:
    tb_window       windows;
    tb_ordering     orderings;
    tb_ordered      ordereds;
    tb_datafeeder   datafeeders;
    tb_config       configs;
};